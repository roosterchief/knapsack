package dk.radoslav.data;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;

public class InMemoryDatabase {
	
	/*
	 * NB! Potentially, this storage can lead to memory leaks.
	 * A solution will be a cleanup job of old or already consumed tasks.
	 */
	
	public static final ConcurrentMap<String, TaskDetails> MapTaskDetails = new ConcurrentHashMap<String, TaskDetails>(); 
	public static final ConcurrentMap<String, Task> MapTasks = new ConcurrentHashMap<String, Task>();

}
