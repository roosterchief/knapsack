package dk.radoslav.engine;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dk.radoslav.model.Problem;
import dk.radoslav.model.Solution;
import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;
import dk.radoslav.repository.TaskRepository;

public class TaskThread extends Thread {

	private static final ExecutorService executor = Executors.newSingleThreadExecutor(); // Yes, make it a bit slow by using single thread..
	
	private TaskRepository taskRepository;
	private Problem problem;
	private String taskId;

	public String getTaskId() {
		return taskId;
	}

	public TaskThread(Problem problem, TaskRepository taskRepository) {
		this.problem = problem;
		this.taskRepository = taskRepository;

		init();
	}

	private void init() {
		taskId =  UUID.randomUUID().toString();

		Task task = new Task(taskId);
		
		try {
			taskRepository.insertTask(taskId, task);
		} catch (Throwable e) {			
			e.printStackTrace();
			return;
		}
		
		TaskDetails taskDetails = new TaskDetails(taskId, problem);
		
		try {
			taskRepository.insertTaskDetails(taskId, taskDetails);
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
		
		executor.execute(this); // it will kick off the run() method.
	}

	public void run() {
		long start = System.currentTimeMillis();

		try {
			taskRepository.updateTaskStatus(taskId, "started");
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
		int[] items = Knapsack.solve(problem.getCapacity(), problem.getWeights(), problem.getValues());

		long end = System.currentTimeMillis();

		Solution solution = new Solution(items, end - start);
		
		try {
			taskRepository.resolveTask(taskId, solution);
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
		try {
			taskRepository.updateTaskStatus(taskId, "completed");
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}
	}

	public static void shutdown() {
		executor.shutdownNow();
	}
}