package dk.radoslav.engine;

// Based on https://introcs.cs.princeton.edu/java/23recursion/Knapsack.java.html
public class Knapsack {

	public static int[] solve(int capacity, int[] weights, int[] values) {

		int N = weights.length;
		int W = capacity;

		// check equal weights size and values size.
		if (weights.length != values.length) {
			return null;
		}

		// check all weights, values and capacity are positive integers.
		if (W <= 0) {
			return null;
		}
		for (int n = 0; n < N; n++) {
			if (weights[n] < 0 || values[n] < 0) {
				return null;
			}
		}

		// shift indices so we have items 1..N (we might want to be more
		// user-friendly)
		int[] profit = new int[N + 1];
		int[] weight = new int[N + 1];
		for (int n = 1; n <= N; n++) {
			profit[n] = values[n - 1];
			weight[n] = weights[n - 1];
		}

		// opt[n][w] = max values of packing items 1..n with weight limit w.
		// sol[n][w] = does opt solution to pack items 1..n with weight limit w
		// include item n?
		int[][] opt = new int[N + 1][W + 1];
		boolean[][] sol = new boolean[N + 1][W + 1];

		for (int n = 1; n <= N; n++) {
			for (int w = 1; w <= W; w++) {

				// don't take item n
				int option1 = opt[n - 1][w];

				// take item n
				int option2 = Integer.MIN_VALUE;
				if (weight[n] <= w)
					option2 = profit[n] + opt[n - 1][w - weight[n]];

				// select better of two options
				opt[n][w] = Math.max(option1, option2);
				sol[n][w] = (option2 > option1);
			}
		}

		// determine which items to take.
		int takeCnt = 0;
		boolean[] take = new boolean[N + 1];
		for (int n = N, w = W; n > 0; n--) {
			if (sol[n][w]) {
				take[n] = true;
				w = w - weight[n];
				takeCnt++;
			} else {
				take[n] = false;
			}
		}

		// prepare result array.
		int[] res = new int[takeCnt];
		takeCnt = 0;

		// print results and setup result array.
		// System.out.println("item" + "\t" + "profit" + "\t" + "weight" + "\t"
		// + "take");
		for (int n = 1; n <= N; n++) {
			// System.out.println(n + "\t" + profit[n] + "\t" + weight[n] + "\t"
			// + take[n]);
			if (take[n]) {
				res[takeCnt] = n - 1;
				takeCnt++;
			}
		}

		return res;
	}
}
