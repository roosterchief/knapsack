package dk.radoslav;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Main {
	public static boolean UNIT_TESTS_RUNNING = false;
    public static final String BASE_URI = "http://0.0.0.0:8080/knapsack/";
	
    private static HttpServer server;

    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig().packages("dk.radoslav.resource");

        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
    	server = startServer();
        
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
            	if(server != null){
            		server.shutdownNow();
            	}
            }
        }));
        
        System.out.println(String.format("Application started.\nTry out %s\nStop the application using CTRL+C", BASE_URI));

        Thread.currentThread().join();
    }
    
    public static void stopServer() {
    	if(server != null){
    		server.shutdownNow();
    	}
    	
    	try {
			Thread.sleep(2000); // Give a little time to send the response back through the wire before the server is stopped.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	System.exit(0);
    }
}

