package dk.radoslav.resource;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dk.radoslav.Main;
import dk.radoslav.engine.TaskThread;
import dk.radoslav.model.Tasks;

@Path("admin")
public class AdminResource extends ResourceBase {

	@POST
	@Path("shutdown")
	public void shutdown() {
		TaskThread.shutdown(); // Stop accepting new tasks by stopping the executor service.
		Main.stopServer(); // Stop http server by calling shutdown on the server instance.
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("tasks")
	public Response getAllTasks() {
		try {
			Tasks allTasks = taskRepository.findAllTasks();
			return Response.ok().entity(allTasks).build();			
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
