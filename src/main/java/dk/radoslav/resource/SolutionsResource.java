package dk.radoslav.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dk.radoslav.model.TaskDetails;

@Path("solutions")
public class SolutionsResource extends ResourceBase{
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{taskId}")
	public Response getSolution(@PathParam("taskId") String taskId) {
		try {
			TaskDetails taskDetails = taskRepository.findSolution(taskId);
			
			if(taskDetails == null){
				return Response.status(Status.NOT_FOUND).build();
			}
			
			return Response.ok().entity(taskDetails).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
				
		
	}
}
