package dk.radoslav.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import dk.radoslav.model.Problem;
import dk.radoslav.model.Task;

@Path("tasks")
public class TasksResource extends ResourceBase{
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createTask(Problem problem) {
		if(problem == null || problem.getCapacity() <= 0
				|| problem.getValues() == null || problem.getWeights() == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
				
		try {
			Task task = taskRepository.createFrom(problem);
			return Response.ok().entity(task).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}		
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{taskId}")
	public Response getTask(@PathParam("taskId") String taskId) {
		try {
			Task task = taskRepository.findTask(taskId);
			
			if(task == null){
				return Response.status(Status.NOT_FOUND).build();
			}
			return Response.ok().entity(task).build();
		} catch (Throwable e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}		
	}
}
