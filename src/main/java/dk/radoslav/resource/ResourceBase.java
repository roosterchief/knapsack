package dk.radoslav.resource;

import dk.radoslav.Main;
import dk.radoslav.repository.TaskRepository;
import dk.radoslav.repository.TaskRepositoryMongoDB;
import dk.radoslav.repository.TaskRepositoryStub;

public abstract class ResourceBase {
	
	protected TaskRepository taskRepository = (Main.UNIT_TESTS_RUNNING) ? new TaskRepositoryStub() : new TaskRepositoryMongoDB(); 
}
