package dk.radoslav.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.radoslav.model.Tasks;

public class AdminClient extends ClientBase{
		
	public Tasks get () {		
		Response response = target.path("admin/tasks").request(MediaType.APPLICATION_JSON).get(Response.class);	
		return response.readEntity(Tasks.class);
	}
	
	public void shutdown() {
		Response response = target.path("admin/shutdown")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(null, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != 204) {
			throw new RuntimeException(response.getStatus() + ": there was an error on the server.");
		}
	}
}
