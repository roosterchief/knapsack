package dk.radoslav.client;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.radoslav.model.TaskDetails;

public class SolutionsClient extends ClientBase{
	
	public TaskDetails get(String id) {
		Response response = target.path("solutions/" + id).request(MediaType.APPLICATION_JSON).get(Response.class);
		
		if(response.getStatus() != 200) {
			throw new RuntimeException(response.getStatus() + ": there was an error on the server.");
		}
	
		return response.readEntity(TaskDetails.class);
	}
}
