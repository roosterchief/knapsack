package dk.radoslav.client;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.radoslav.model.Problem;
import dk.radoslav.model.Task;

public class TasksClient extends ClientBase {
	
	public Task create(Problem problem) {
		Response response = target.path("tasks")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(problem, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != 200) {
			throw new RuntimeException(response.getStatus() + ": there was an error on the server.");
		}
		
		return response.readEntity(Task.class);
	}
	
	public Task get(String id) {
		Response response = target.path("tasks/" + id)
				.request(MediaType.APPLICATION_JSON)
				.get(Response.class);
		
		if(response.getStatus() != 200) {
			throw new RuntimeException(response.getStatus() + ": there was an error on the server.");
		}
		
		return response.readEntity(Task.class);
	}
}
