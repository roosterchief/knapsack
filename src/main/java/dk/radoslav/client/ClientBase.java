package dk.radoslav.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import dk.radoslav.Main;

public abstract class ClientBase {

	protected Client client;
	protected WebTarget target;

	public ClientBase () {
		client = ClientBuilder.newClient();
		target = client.target(Main.BASE_URI);
	}
}
