package dk.radoslav.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskDetails {

	private String taskId;
	private Problem problem;
	private Solution solution;

	public TaskDetails() {
	}

	public TaskDetails(String taskId, Problem problem) {
		this();
		this.taskId = taskId;
		this.problem = problem;
	}

	public void resolve(Solution solution) {
		this.solution = solution;
	}

	@Override
	public String toString() {
		return "TaskData [taskId=" + taskId + ", problem=" + problem
				+ ", solution=" + solution + "]";
	}

	@XmlElement(name = "task")
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@XmlElement(name = "problem")
	public Problem getProblem() {
		return problem;
	}
	public void setProblem(Problem problem) {
		this.problem = problem;
	}
	
	@XmlElement(name = "solution")
	public Solution getSolution() {
		return solution;
	}
	public void setSolution(Solution solution) {
		this.solution = solution;
	}
}