package dk.radoslav.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Task {

	private String id;
	private String status; // one of "submitted", "started" and "completed"
	private TaskTimestamp timestamps;

	public Task(){
		timestamps = new TaskTimestamp();
	}
	
	public Task(String id) {
		this();
		this.id = id;
		timestamps.setSubmitted(System.currentTimeMillis());
		status = "submitted";
	}

	public void update(String status) {
		if (status.equals("submitted")){
			timestamps.setSubmitted(System.currentTimeMillis());
		} else if (status.equals("started")){
			timestamps.setStarted(System.currentTimeMillis());
		} else if (status.equals("completed")){
			timestamps.setCompleted(System.currentTimeMillis());
		} else{
			throw new RuntimeException("Invalid status:" + status);
		}
		
		this.status = status;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", status=" + status + ", timestamps=" + timestamps + "]";
	}

	@XmlElement(name="task")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name="status")
	public String getStatus() {
		return status;
	}	
	public void setStatus(String status) {
		this.status = status;
	}

	public TaskTimestamp getTimestamps() {
		return timestamps;
	}	
	public void setTimestamps(TaskTimestamp timestamps){
		this.timestamps = timestamps;
	}
}