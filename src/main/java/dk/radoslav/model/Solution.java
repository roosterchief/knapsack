package dk.radoslav.model;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Solution {

	private int[] items;
	private long time;

	public Solution(){
	}
	
	public Solution(int[] items, long time) {
		this();
		this.items = items;
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "Solution [items=" + Arrays.toString(items) + ", time=" + time + "]";
	}
	
	@XmlElement(name="items")
	public int[] getItems() {
		return items;
	}
	public void setItems(int[] items) {
		this.items = items;
	}

	@XmlElement(name="time")
	public long getTime() {
		return time;
	}	
	public void setTime(long time) {
		this.time = time;
	}
}