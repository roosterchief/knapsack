package dk.radoslav.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Tasks {

	private Task[] submitted;
	private Task[] started;
	private Task[] completed;
	
	public Tasks(){
	}
		
	public Tasks(Collection<Task> allTasks) {
		this();
		sortTasks(allTasks);
	}

	private void sortTasks(Collection<Task> allTasks) {
		List<Task> submittedTemp = new ArrayList<Task>(); 
		List<Task> startedTemp = new ArrayList<Task>();
		List<Task> completedTemp = new ArrayList<Task>();
		
		for (Task task : allTasks) {
			if(task.getStatus().equals("submitted")){
				submittedTemp.add(task);
			} else if(task.getStatus().equals("started")){
				startedTemp.add(task);
			} else if(task.getStatus().equals("completed")){
				completedTemp.add(task);
			}
		}
		
		submitted = new Task[submittedTemp.size()];
		submitted = submittedTemp.toArray(submitted);
		
		started = new Task[startedTemp.size()];
		started = startedTemp.toArray(started);
		
		completed = new Task[completedTemp.size()];
		completed = completedTemp.toArray(completed);		
	}

	@XmlElement(name="submitted")
	public Task[] getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Task[] submitted) {
		this.submitted = submitted;
	}
	
	@XmlElement(name="started")
	public Task[] getStarted() {
		return started;
	}
	public void setStarted(Task[] started) {
		this.started = started;
	}
	
	@XmlElement(name="completed")
	public Task[] getCompleted() {
		return completed;
	}
	public void setCompleted(Task[] completed) {
		this.completed = completed;
	}	
}
