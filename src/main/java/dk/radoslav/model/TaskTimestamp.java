package dk.radoslav.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskTimestamp{
	
	private long submitted = -1L;
	private long started = -1L;
	private long completed = -1L;
	
	public TaskTimestamp(){			
	}
	
	@XmlElement(name="submitted")
	public long getSubmitted() {
		return submitted;
	}
	public void setSubmitted(long submitted) {
		this.submitted = submitted;
	}
	
	@XmlElement(name="completed")
	public long getCompleted() {
		return completed;
	}
	public void setCompleted(long completed) {
		this.completed = completed;
	}
	
	@XmlElement(name="started")
	public long getStarted() {
		return started;
	}
	public void setStarted(long started) {
		this.started = started;
	}
	
	@Override
	public String toString() {
		return "Timestamps [submitted=" + submitted + ", started="
				+ started + ", completed=" + completed + "]";
	}
}
