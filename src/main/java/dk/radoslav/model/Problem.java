package dk.radoslav.model;

import java.util.Random;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Problem {

	private int capacity;
	private int[] weights;
	private int[] values;

	public Problem(){		
	}
		
	public Problem(int capacity, int[] weights, int[] values) {
		this();
		this.capacity = capacity;
		this.weights = weights;
		this.values = values;
	}

	public static Problem generateProblem() {
		Random r = new Random();

		int capacity = r.nextInt(1000);
		int size = r.nextInt(10);

		int[] weights = new int[size];
		int[] values = new int[size];

		for (int i = 0; i < size; i++) {
			weights[i] = r.nextInt(10);
			values[i] = r.nextInt(10);
		}

		return new Problem(capacity, weights, values);
	}

	@Override
	public String toString() {
		return "Problem [capacity=" + capacity + ", weights="
				+ Arrays.toString(weights) + ", values="
				+ Arrays.toString(values) + "]";
	}
    
	@XmlElement(name="capacity")
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	@XmlElement(name="weights")
	public int[] getWeights() {
		return weights;
	}	
	public void setWeights(int[] weights) {
		this.weights = weights;
	}

	@XmlElement(name="values")
	public int[] getValues() {
		return values;
	}
	public void setValues(int[] values) {
		this.values = values;
	}

}