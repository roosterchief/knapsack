package dk.radoslav.repository;

import dk.radoslav.model.Problem;
import dk.radoslav.model.Solution;
import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;
import dk.radoslav.model.Tasks;

public interface TaskRepository {

	Task createFrom(Problem problem) throws Throwable;

	Task findTask(String taskId) throws Throwable;

	Tasks findAllTasks() throws Throwable;

	TaskDetails findSolution(String taskId) throws Throwable;
	
	void updateTaskStatus(String taskId, String status) throws Throwable;
	
	void insertTask(String taskId, Task task) throws Throwable;
	
	void resolveTask(String taskId, Solution solution) throws Throwable;
	
	void insertTaskDetails(String taskId, TaskDetails taskDetails) throws Throwable;
}
