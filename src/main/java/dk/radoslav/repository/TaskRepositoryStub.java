package dk.radoslav.repository;

import java.util.Collection;

import dk.radoslav.data.InMemoryDatabase;
import dk.radoslav.engine.TaskThread;
import dk.radoslav.model.Problem;
import dk.radoslav.model.Solution;
import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;
import dk.radoslav.model.Tasks;

public class TaskRepositoryStub implements TaskRepository {

	@Override
	public Task createFrom(Problem problem) {
		TaskThread taskThread = new TaskThread(problem, this);
		String taskId = taskThread.getTaskId();
		Task task = InMemoryDatabase.MapTasks.get(taskId);
		return task;
	}

	@Override
	public Task findTask(String taskId) {
		Task task = InMemoryDatabase.MapTasks.get(taskId);
		return task;
	}

	@Override
	public Tasks findAllTasks() {
		Collection<Task> allTasks = InMemoryDatabase.MapTasks.values();
		Tasks sortedTasks = new Tasks(allTasks);
		return sortedTasks;
	}
	
	@Override
	public TaskDetails findSolution(String taskId) {
		TaskDetails taskDetails = InMemoryDatabase.MapTaskDetails.get(taskId);
		return taskDetails;
	}

	@Override
	public void updateTaskStatus(String taskId, String status) {
		InMemoryDatabase.MapTasks.get(taskId).update(status);				
	}

	@Override
	public void insertTask(String taskId, Task task) {
		InMemoryDatabase.MapTasks.put(taskId, task);		
	}

	@Override
	public void resolveTask(String taskId, Solution solution) {
		InMemoryDatabase.MapTaskDetails.get(taskId).resolve(solution);		
	}

	@Override
	public void insertTaskDetails(String taskId, TaskDetails taskDetails) {
		InMemoryDatabase.MapTaskDetails.put(taskId, taskDetails);		
	}
}
