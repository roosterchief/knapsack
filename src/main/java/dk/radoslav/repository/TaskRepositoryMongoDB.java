package dk.radoslav.repository;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

import dk.radoslav.engine.TaskThread;
import dk.radoslav.model.Problem;
import dk.radoslav.model.Solution;
import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;
import dk.radoslav.model.Tasks;


@SuppressWarnings("deprecation")
public class TaskRepositoryMongoDB implements TaskRepository{

	private ObjectMapper mapper;
	private MongoClient mongoClient;
	private MongoCollection<Document> taskCollection;
	private MongoCollection<Document> taskDetailsCollection;
	
	public TaskRepositoryMongoDB() {
		mapper = new ObjectMapper();
		mongoClient = new MongoClient("mongodb");		
		
		MongoDatabase database = mongoClient.getDatabase("knapsack");
		
		taskCollection = database.getCollection("task");
		taskDetailsCollection = database.getCollection("taskdetails");
	}
	
	@Override
	public Task createFrom(Problem problem) throws Throwable {
		TaskThread taskThread = new TaskThread(problem, this);
		String taskId = taskThread.getTaskId();
		
		return findTask(taskId);		
	}

	@Override
	public Task findTask(String taskId) throws Throwable {
		Document taskDocument = taskCollection.find(eq("id", taskId)).first();
		taskDocument.remove("_id"); // Otherwise cannot deserialize document to POJO.
		String json = JSON.serialize(taskDocument); // Document.toJson() does not work, it has a bug.
		
		Task task = mapper.readValue(json, Task.class);
		return task;			
	}

	@Override
	public Tasks findAllTasks() throws Throwable{
		List<Task> allTasks = new ArrayList<>();
		MongoCursor<Document> cursor = taskCollection.find().iterator();
		
		try {
		    while (cursor.hasNext()) {
		        Document taskDocument = cursor.next();
		        taskDocument.remove("_id"); // Otherwise cannot deserialize document to POJO.
		        String json = JSON.serialize(taskDocument); // Document.toJson() does not work, it has a bug.
		        
				Task task = mapper.readValue(json, Task.class);
				allTasks.add(task);				
		    }
		} finally {
		    cursor.close();
		}
		Tasks sortedTasks = new Tasks(allTasks);
		return sortedTasks;
	}


	@Override
	public TaskDetails findSolution(String taskId) throws Throwable {
		Document taskDetailsDocument = taskDetailsCollection.find(eq("taskId", taskId)).first();
		taskDetailsDocument.remove("_id"); // Otherwise cannot deserialize document to POJO.
		String json = JSON.serialize(taskDetailsDocument); // Document.toJson() does not work, it has a bug.
		
		TaskDetails task = mapper.readValue(json, TaskDetails.class);
		return task;		
	}

	@Override
	public void updateTaskStatus(String taskId, String status) throws Throwable {
		Task task = findTask(taskId);
		task.update(status);
		
		String json = mapper.writeValueAsString(task);
		Document taskDocument = Document.parse(json);
		taskCollection.updateOne(eq("id", taskId), new Document("$set", taskDocument));			
	}

	@Override
	public void insertTask(String taskId, Task task) throws Throwable {
		
		String json = mapper.writeValueAsString(task);
		Document taskDocument = Document.parse(json);
		taskCollection.insertOne(taskDocument);					
	}

	@Override
	public void resolveTask(String taskId, Solution solution) throws Throwable {
		TaskDetails taskDetails = findSolution(taskId);
		taskDetails.resolve(solution);
		
		String json = mapper.writeValueAsString(taskDetails);
		Document taskDetailsDocument = Document.parse(json);
		taskDetailsCollection.updateOne(eq("taskId", taskId), new Document("$set", taskDetailsDocument));			
	}

	@Override
	public void insertTaskDetails(String taskId, TaskDetails taskDetails) throws Throwable {		
		String json = mapper.writeValueAsString(taskDetails);
		Document taskDetailsDocument = Document.parse(json);
		taskDetailsCollection.insertOne(taskDetailsDocument);							
	}
}
