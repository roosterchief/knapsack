package dk.radoslav;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

import dk.radoslav.client.AdminClient;
import dk.radoslav.client.TasksClient;
import dk.radoslav.model.Problem;
import dk.radoslav.model.Tasks;

public class AdminClientTest extends TestBase{

	@Test
	public void testGet() {
		// Arrange
		TasksClient client1 = new TasksClient();
		Problem problem = Problem.generateProblem();
		client1.create(problem);
		AdminClient client2 = new AdminClient();
		
		// Act
		Tasks target = client2.get();
		
		// Assert
		assertNotNull(target);
	}

	@Test
	@Ignore
	public void testShutdown() {
		// Arrange
		AdminClient client = new AdminClient();
		
		// Act
		client.shutdown();
	}

}
