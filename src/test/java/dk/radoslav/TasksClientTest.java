package dk.radoslav;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.radoslav.client.TasksClient;
import dk.radoslav.model.Problem;
import dk.radoslav.model.Task;

public class TasksClientTest extends TestBase{

	@Test
	public void testCreate() {
		// Arrange
		TasksClient client = new TasksClient();
		Problem problem = Problem.generateProblem();

		// Act
		Task target = client.create(problem);
		
		// Assert
		assertNotNull(target);
	}
	
	@Test(expected=RuntimeException.class)
	public void testCreate_WhenBadData() {
		// Arrange
		TasksClient client = new TasksClient();
		Problem problem = null;

		// Act
		client.create(problem);
	}

	@Test
	public void testGet() {
		// Arrange
		TasksClient client = new TasksClient();
		Problem problem = Problem.generateProblem();
		Task task = client.create(problem);
		String id = task.getId();
				
		// Act
		Task target = client.get(id);
		
		// Assert
		assertNotNull(target);
	}

	@Test(expected=RuntimeException.class)
	public void testGet_WhenTaskIdDoesNotExist() {
		// Arrange 
		TasksClient client = new TasksClient();
		
		// Act
		client.get("not-existing-id");
	}
}
