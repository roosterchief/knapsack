package dk.radoslav;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;

public abstract class TestBase {

    private HttpServer server;
    
    @Before
    public void setUp() throws Exception {
    	Main.UNIT_TESTS_RUNNING = true;
        server = Main.startServer();
    }

    @SuppressWarnings("deprecation")
	@After
    public void tearDown() throws Exception {
        server.stop();
    }
}
