package dk.radoslav;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.radoslav.client.SolutionsClient;
import dk.radoslav.client.TasksClient;
import dk.radoslav.model.Problem;
import dk.radoslav.model.Task;
import dk.radoslav.model.TaskDetails;

public class SolutionsClientTest extends TestBase{

	@Test
	public void testGet() {
		// Arrange
		TasksClient client1 = new TasksClient();
		SolutionsClient client2 = new SolutionsClient();
		Problem problem = Problem.generateProblem();
		Task task = client1.create(problem);
		String id = task.getId();
				
		// Act
		TaskDetails target = client2.get(id);
		
		// Assert
		assertNotNull(target);
	}
	
	@Test(expected=RuntimeException.class)
	public void testGet_WhenTaskIdDoesNotExist() {
		// Arrange
		SolutionsClient client = new SolutionsClient();
		
		// Act
		client.get("not-existing-id");
	}
}
