# Documentation

## Purpose
The following document describes the most important points in the solution of the Knapsack Optimization Service problem.   

## Used Technologies
The code and the deployed service are using the following technologies:  

1. [MongoDB](https://www.mongodb.com/)
2. [Maven](https://maven.apache.org/)
3. [Java 8](http://www.oracle.com/technetwork/java/javaee/downloads/java-ee-sdk-downloads-3908423.html)
4. [JAX-RS / Jersey](https://jersey.github.io/index.html)
5. [Gizzly](https://javaee.github.io/grizzly/)
6. [JUnit](http://junit.org/)
7. [Mongo Java Driver](https://mongodb.github.io/mongo-java-driver/)
8. [Jackson](https://github.com/FasterXML/jackson)
9. [Docker](https://www.docker.com/)
10. [Azure](https://azure.microsoft.com/en-gb/)
11. [Eclipse](https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2) 

## Getting Started
If you want to build and run the solution do the following steps:

>Alternatively, you can use the already deployed service here: 
>```
>http://myapp2-nomongodb.78486bd2.svc.dockerapp.io:18080/knapsack
>```
>or here:
>```
>http://api.roosterchiefstack.238223a2.svc.dockerapp.io:28080/knapsack
>```

>_NB: If you shutdown the online services it will NOT restart automatically!_

1. Download the code from the BitBucket repoistiry at:
```
git clone https://roosterchief@bitbucket.org/roosterchief/knapsackoptimizerservice.git
```
2. Install the Docker CE (Community Edition) 
```
https://www.docker.com/get-docker
```
3. Build the image for the API by using this command: 
```
docker build -f knapsack.Dockerfile -t <docker_username>/<image_name> .
```
4. Run the containers

```
docker-compose up
```
or just
```
docker run -d -p 18080:8080 <docker_username>/<image_name>
```
when doing unit tests or the solution will use only the in-memory database. 

5. Use the test script below to test the solution

## Architecture
The following diagram outlines the proposed solution:

```
                  +---------------------------------------------------------------+
                  | TasksResource.java                                            |
                  |                                                               |
                  | +-----------------------------------------------------------+ |
+-------------->  | | TaskRepository.java                                       | |
                  | |                                                           | |
                  | | +-------------------------+  +--------------------------+ | |
                  | | | TaskThread.java         |  | InMemoryDatabase.java    | | |
+-------------->  | | |                         |  | -or-                     | | |
                  | | | +---------------------+ |  | MongoDB                  | | |
                  | | | | Knapsack.java       | |  |                          | | |
                  | | | |                     | |  |                          | | |
+-------------->  | | | +---------------------+ |  |                          | | |
                  | | +-------------------------+  +--------------------------+ | |
                  | |                                                           | |
                  | +-----------------------------------------------------------+ |
                  |                                                               |
                  +---------------------------------------------------------------+
```
When a request is received by the API, the ```TasksResource.java``` or other "controller" class will handle it. These top-level resource-classes use the ```TaskRepository.java``` which abstracts all operations related to save and load data from the storage.

Currently, there are two implementations of ```TaskRepository.java``` - ```InMemoryDatabase.java``` and ```TaskRepositoryMongoDB.java``` 

Most of the operations will be handled directly in ```TaskRepository.java``` with exception of one - the calculations for the algorithmic part (the Knapsack problem).  

When the Knapsack task is submitted the ```TaskRepository.java``` will spin a thread - ```TaskThread.java```. The last will handle the communication with the Knapsack solver - ```Knapsack.java``` and the needed updates to the storage (different statuses, timestamps, found solution).

The in-memory database option uses ```ConcurrentHashMap``` for data storage. 

The control over the running threads is done by ```ExecutionService``` class. In order to experience some delay the execution service is configured to work with one thread only.

There are also some model classes in the package: ```dk.radoslav.model```. These classes model the incoming requests data, the response data and ensure the entities saved in the database. 

## API Endpoints and Returned Status Codes

* ```POST /tasks``` can return status code 200, 400, 500
* ```GET /tasks/<taskId>``` can return status code 200, 404, 500
* ```GET /solutions/<taskId>``` can return status code 200, 404, 500
* ```GET /admin/alltasks``` can return status code 200, 500
* ```POST /admin/shutdown``` can return status code 204

## Algorithm Complexity

The used algorithm is based on https://introcs.cs.princeton.edu/java/23recursion/Knapsack.java.html
The complexity is ```O(nW)``` where ```n``` is the number of the distinct items and ```W``` is the knapsack capacity. Ref: [Wikipedia](https://en.wikipedia.org/wiki/Knapsack_problem)

## Unit Tests

There is a minimal amount of unit tests available in ```src/test/java```.

## Test Script

```bash
$ curl -XPOST -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/tasks \
   -d '{"capacity": 60, "weights": [10, 20, 33], "values": [10, 3, 30]}'
{"task": "89b1d4a4-608a-4737-9956-9ea85de9b07e", "status": "submitted", "timestamps": {"submitted": 1505225308, "started": -1, "completed": -1}}

$ curl -XGET -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/tasks/89b1d4a4-608a-4737-9956-9ea85de9b07e
{"task": "89b1d4a4-608a-4737-9956-9ea85de9b07e", "status": "submitted", "timestamps": {"submitted": 1505225308, "started": 1505225320, "completed": null}}

$ curl -XGET -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/solutions/not-existing
http status code 404 Not Found

$ curl -XGET -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/tasks/89b1d4a4-608a-4737-9956-9ea85de9b07e
{"task": "89b1d4a4-608a-4737-9956-9ea85de9b07e", "status": "submitted", "timestamps": {"submitted": 1505225308, "started": 1505225320, "completed": 1505225521}}

$ curl -XGET -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/solutions/nbd43jhb
{"task": "89b1d4a4-608a-4737-9956-9ea85de9b07e", "problem": {...}, "solution": {"items": [0, 2]}, "time": 201}

$ curl -XGET -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/admin/tasks
{"completed":[{"task":"42c903fa-4d3e-4fe4-81b0-e2585923f2af","status":"completed","timestamps":{"completed":1512417189098,"started":1512417189098,"submitted":1512417189097}},{"task":"89b1d4a4-608a-4737-9956-9ea85de9b07e","status":"completed","timestamps":{"completed":1512416618356,"started":1512416618355,"submitted":1512416618354}}],"started":[],"submitted":[]}

$ curl -XPOST -H 'Content-type: application/json' http://192.168.99.100:18080/knapsack/admin/shutdown
http status code 204 no content and the service is shutting down...
```

## Exception Handling

The code has minimal amount of ```try\catch``` blocks. There are 3 places which will do exception handling:

* ```Main.java``` - 1 ocurrance
* ```*Repository.java``` - Handles unexpected scenarios during the processing of REST requests. Mostly, the exception handling code is necesary because ```TaskRepository.java``` which works with the datastorage but does not handle Exceptions.  
* ```TaskThread.java``` - Again, because it works with ```TaskRepository.java``. 

## Logging

The code does not use any logging libraray. The handled exceptions will be printed in the console. The only log capability is the log of the Docker container. The stacktraces of the exceptions should be printed there. 

## Limitations

(*) The request JSON for Problem entity is not the same as the specification. When the timestams are not defined they appear as '-1' instead 'null'. The response JSON for Admin/Tasks is not the same as the specification. S. the test script section to overcome the possible problems.

S. Test Script section to overcome the problem. 

(**) Possible memory heap problems: If the calculated problems are huge it is possible to run out of memory or heap space.

